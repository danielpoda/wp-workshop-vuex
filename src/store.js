import Vue from "vue";
import Vuex from "vuex";
import * as ENUM from "./storeEnums";
import { getTodos, addTodo, removeTodo } from "./firedb";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todos: [],
    todoState: ENUM.TODO_STATE.INIT
  },
  mutations: {
    updateTodos(state, { todos }) {
      state.todos = todos;
    },
    updateTodoState(state, { todoState }) {
      state.todoState = todoState;
    }
  },
  actions: {
    [ENUM.GET_TODOS]({ commit, state }) {
      if (state.todoState !== ENUM.TODO_STATE.LOADING) {
        commit("updateTodoState", { todoState: ENUM.TODO_STATE.LOADING });
        getTodos().then(todos => {
          console.log(todos);
          commit("updateTodoState", { todoState: ENUM.TODO_STATE.OK });
          commit("updateTodos", { todos });
        });
      }
    },
    [ENUM.ADD_TODO]({ dispatch }, todoItem) {
      addTodo(todoItem).then(() => {
        dispatch(ENUM.GET_TODOS);
      });
    },
    [ENUM.DELETE_TODO]({ dispatch }, key) {
      removeTodo(key).then(() => {
        dispatch(ENUM.GET_TODOS);
      });
    }
  },
  getters: {
    todos: state => {
      return state.todos;
    }
  }
});
