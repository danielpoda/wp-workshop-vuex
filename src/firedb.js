import * as firebase from "firebase";

const config = {
  apiKey: "AIzaSyB_6jCXJa6PJi37FiHaXaBRX3ZAEAD2FmY",
  authDomain: "catcon-5b2a8.firebaseapp.com",
  databaseURL: "https://catcon-5b2a8.firebaseio.com/",
  projectId: "catcon-5b2a8"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const todosRef = firebase.database().ref("ws-todos");

/**
 * watch for change
 */
export const watchTodos = listener => {
  todosRef.on("value", snapshot => {
    const snapshotObj = snapshot.val();
    let todos =
      Object.keys(snapshotObj).map(key => {
        return { key, ...snapshotObj[key] };
      }) || [];
    listener(todos);
  });
};

/**
 * get once
 * @returns {Array} array of todo objects
 */
export const getTodos = () => {
  return todosRef.once("value").then(snapshot => {
    const snapshotObj = snapshot.val();
    if (snapshotObj !== null)
      return Object.keys(snapshotObj).map(key => {
        return { key, ...snapshotObj[key] };
      });
    return [];
  });
};

/**
 * add item
 * @param {Object} obj
 * @param {String} obj.name
 */
export const addTodo = todoItem => {
  const ts = Date.now(); // generate timestamp
  return todosRef.push().set({ ...todoItem, ts });
};

/**
 * update item
 * @param {Object} item
 */
export const updateTodo = item => {
  const { key } = item;
  const nItem = { ...item };
  // remove key
  delete nItem.key;
  // update item
  todosRef.child(key).update(nItem);
};

/**
 * remove item
 * @param {String} key
 */
export const removeTodo = key => {
  console.log(key);
  return todosRef.child(key).remove();
};
