// export const MUTATION_INCREMENT = "increment";
// export const MUTATION_COUNTER_UPDATING = "counter updating";
// export const MUTATION_COUNTER_UPDATED = "counter updated";
export const GET_TODOS = "get todos";
export const ADD_TODO = "add todo";
export const DELETE_TODO = "delete todo";

export const TODO_STATE = {
  INIT: "init",
  LOADING: "loading",
  ERROR: "error",
  OK: "ok"
};
